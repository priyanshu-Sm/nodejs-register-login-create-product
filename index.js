var express = require("express");
var router = express.Router();
var userModel = require("./users");
var passport = require("passport");
var expressSession = require('express-session')
var localStrategy = require("passport-local");
var productModel = require("./product");

passport.use(new localStrategy(userModel.authenticate()));

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index");
});

router.post("/register", function (req, res) {
  var newUser = new userModel({
    name: req.body.name,
    username: req.body.username,
  });
  userModel
    .register(newUser, req.body.password)
    .then(function (u) {
      passport.authenticate("local")(req, res, function () {
        res.redirect("/profile");
      });
    })
    .catch(function (e) {
      res.send(e);
    });
});

// router.get('/profile', function(req, res){
//   productModel.find().then(function(data){
//     res.render('profile', { data : data})
//   })
// })

router.get('/profile', isLoggedIn,function(req, res){
  userModel.findOne({username : req.session.passport.user})
  .populate('product')
  .then(function(founduser){
    console.log(founduser)
    productModel.find()
    .then(function(foundProduct){
      res.render('profile', {user : founduser,  foundProduct})
    })
 
  })
}) 

router.post("/product", function (req, res) {
  userModel.findOne({username : req.session.passport.user})
  .then(function(founduser){
    productModel.create({
      name: req.body.name,
      image: req.body.image,
      description: req.body.description,
      price: req.body.price,
      userseller : founduser
    })
    .then(function (createdProduct) {
      console.log(createdProduct)
      founduser.product.push(createdProduct)
      founduser.save()
      .then(function(){
        res.redirect('/profile');
      })
    })
    .catch(function (err) {
      res.send(err);
    });
  })
 
});

router.get('/delete/:id', function(req, res){
  productModel.findByIdAndDelete({_id : req.params.id})
  .then(function(productDelete){
    res.redirect('/profile')
  }).catch(function(err){
    res.send(err)
  })
})


router.get('/edit/:id', function(req, res){
  productModel.findOne({_id : req.params.id})
  .then(function(data) {
    res.render('updatepage', {data})
  })
})

router.post('/edit/:id', function(req, res){
  productModel.findByIdAndUpdate({_id : req.params.id},
    {name : req.body.name,
    image : req.body.image,
    description : req.body.description,
    price : req.body.price},
    {new : true})
    .then(function(productUpdated){
    res.redirect('/profile')
  }).catch(function(err){
    res.send(err)
  })
})

router.post("/login",
  passport.authenticate("local", {
    successRedirect: "/profile",
    failureRedirect: "/",
  }),
  function (req, res, next) {}
);

router.get("/logout", function (req, res, next) {
  req.logout();
  res.redirect("/");
});

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/");
  }
}
module.exports = router;
