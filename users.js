const mongoose = require('mongoose')
var plm  = require('passport-local-mongoose')

mongoose.connect("mongodb://localhost/datapassport")

var userSchema = mongoose.Schema({
  name :  String,
  username : String,
  password : String,
  product :[{type : mongoose.Schema.Types.ObjectId, ref : 'product'}]
})

userSchema.plugin(plm)

module.exports = mongoose.model('user', userSchema)