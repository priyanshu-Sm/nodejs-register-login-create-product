const mongoose = require('mongoose')


var productSchema = mongoose.Schema({
 name : String,
 image : String,
 description : String,
 price : Number,
 userseller : {
    type : mongoose.Schema.Types.ObjectId,
    ref : 'user'
}
})


module.exports = mongoose.model('product', productSchema)